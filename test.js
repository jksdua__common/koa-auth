/* globals describe, beforeEach, it */

describe('#koa-auth', function() {
  var http = require('http');

  var co = require('co');
  var koa = require('koa-framework');
  var merge = require('lodash.merge');
  var expect = require('chai').expect;
  var request = require('supertest');
  var session = require('koa-header-session');

  var auth = require('@knoxxnxt/auth')();
  var koaAuth = require('./index')(auth);;
  var authRouter;

  var app;

  function *two00() {
    this.status = 200;
  } // jshint ignore:line

  // create simple app
  beforeEach(function() {
    var dummyApp = koa();

    // add session middleware
    dummyApp.use(session({ defer: true }));
    // load user if session exists
    dummyApp.use(koaAuth.middleware.loadUser);
    // add auth api
    authRouter = koaAuth.mount(dummyApp);

    // add routes useful for testing
    var router = dummyApp.router();
    dummyApp.mount(router);

    router.get('/user', koaAuth.middleware.assertState({ authenticated: true }), function *() {
      this.body = this.state.user || {};
      this.status = 200;
    }); // jshint ignore:line
    router.get('/session', koaAuth.middleware.assertState({ authenticated: true }), function *() {
      this.body = (yield this.session) || {};
      this.status = 200;
    });
    router.get('/unauthenticated', koaAuth.middleware.assertState({ authenticated: false }), two00);
    router.get('/role/boss', koaAuth.middleware.assertState({ role: 'boss' }), two00);
    router.get('/role/boss-or-manager', koaAuth.middleware.assertState({ role: ['boss', 'manager'] }), two00);
    router.get('/status/registered', koaAuth.middleware.assertState({ status: auth.constants.STATUS.REGISTERED }), two00);
    router.get('/status/enabled/role/boss', koaAuth.middleware.assertState({ status: auth.constants.STATUS.ENABLED, role: 'boss' }), two00);
    router.get('/status/enabled/role/boss', koaAuth.middleware.assertState({ status: auth.constants.STATUS.ENABLED, role: 'boss' }), two00);
    router.get('/status/registered-or-enabled', koaAuth.middleware.assertState({ status: [auth.constants.STATUS.REGISTERED, auth.constants.STATUS.ENABLED] }), two00);
    router.get('/status/registered-or-enabled/role/boss-or-manager', koaAuth.middleware.assertState({ status: [auth.constants.STATUS.REGISTERED, auth.constants.STATUS.ENABLED], role: ['boss', 'manager'] }), two00);

    app = http.createServer(dummyApp.callback());
  });

  // clear database
  beforeEach(function(done) {
    co(function *() {
      yield auth.db.reset();
    }).then(done, done);
  });

  // add some dummy data
  beforeEach(function(done) {
    co(function *() {
      yield auth.db.bulkInsert([
        {
          email: 'a@a.com',
          hash: '$2a$10$jmUBONzZVmZoWh7GFhIeC.RiDnf.9rRExVO8ActVwdmgKTeJkeODy',
          roles: ['user', 'user admin'],
          status: 'enabled',
          properties: {
            name: 'Person A',
            __pass: 'I like big hashes and I cannot lie'
          }
        },
        {
          email: 'b@b.com',
          hash: '$2a$10$jmUBONzZVmZoWh7GFhIeC.RiDnf.9rRExVO8ActVwdmgKTeJkeODy',
          roles: ['user'],
          status: 'registered',
          activationToken: '1234567890123456789012345678901234567890123456789012345678901234',
          properties: {
            name: 'Person B',
            __pass: 'I like big hashes and I cannot lie'
          }
        },
        {
          email: 'c@c.com',
          hash: '$2a$10$jmUBONzZVmZoWh7GFhIeC.RiDnf.9rRExVO8ActVwdmgKTeJkeODy',
          roles: ['user'],
          status: 'disabled',
          properties: {
            name: 'Person C',
            __pass: 'I like big hashes and I cannot lie'
          }
        }
      ]);
    }).then(done, done);
  });

  describe('#app', function() {
    it('should login a user and set session', function(done) {
      request(app)
        .post('/auth/login')
        .send({ email: 'a@a.com', pass: 'I like big hashes and I cannot lie' })
        .expect(200)
        .expect('X-Session-ID', /.+/)
        .expect(function(res) {
          expect(res.body.properties.name).to.equal('Person A');
        })
        .end(function(err, res) {
          expect(err).to.not.exist; // jshint ignore:line

          request(app)
            .get('/session')
            .set('X-Session-ID', res.get('X-Session-ID'))
            .expect(200)
            .expect(function(res) {
              expect(res.body.email).to.equal('a@a.com');
            })
            .end(done);
        });
    });

    it('should activate a user and set session', function(done) {
      request(app)
        .post('/auth/activate')
        .send({ email: 'b@b.com', token: '1234567890123456789012345678901234567890123456789012345678901234' })
        .expect('X-Session-ID', /.+/)
        .expect(function(res) {
          expect(res.body.properties.name).to.equal('Person B');
        })
        .end(function(err, res) {
          expect(err).to.not.exist; // jshint ignore:line

          request(app)
            .get('/session')
            .set('X-Session-ID', res.get('X-Session-ID'))
            .expect(function(res) {
              expect(res.body.email).to.equal('b@b.com');
            })
            .end(done);
        });
    });

    it('should allow an admin to add a new user', function(done) {
      request(app)
        .post('/auth/login')
        .send({ email: 'a@a.com', pass: 'I like big hashes and I cannot lie' })
        .expect(200)
        .end(function(err, res) {
          expect(err).to.not.exist; // jshint ignore:line

          request(app)
            .post('/auth/raw/insert')
            .set('X-Session-ID', res.get('X-Session-ID'))
            .send({ email: 'd@d.com', pass: 'I like big hashes and I cannot lie' })
            .expect(200)
            .end(done);
        });
    });

    it('should resume a user session', function(done) {
      request(app)
        .post('/auth/login')
        .send({ email: 'a@a.com', pass: 'I like big hashes and I cannot lie' })
        .expect('X-Session-ID', /.+/)
        .end(function(err, res) {
          expect(err).to.not.exist; // jshint ignore:line

          var sessionId = res.get('X-Session-ID');

          request(app)
            .get('/auth/resume')
            .set('X-Session-ID', sessionId)
            .expect(200)
            .expect(function(res) {
              expect(res.body.email).to.equal('a@a.com');
            })
            .end(done);
        });
    });

    it('should logout a user', function(done) {
      request(app)
        .post('/auth/login')
        .send({ email: 'a@a.com', pass: 'I like big hashes and I cannot lie' })
        .expect('X-Session-ID', /.+/)
        .end(function(err, res) {
          expect(err).to.not.exist; // jshint ignore:line

          var sessionId = res.get('X-Session-ID');

          request(app)
            .post('/auth/logout')
            .set('X-Session-ID', sessionId)
            .expect(function(res) {
              expect(res.body).to.eql({ success: true });
            })
            .end(function(err) {
              expect(err).to.not.exist; // jshint ignore:line

              request(app)
                .get('/session')
                .set('X-Session-ID', sessionId)
                .expect(401)
                .end(done);
            });
        });
    });

    it('should allow adding custom middleware', function(done) {
      var dummyApp = koa();

      // add session middleware
      dummyApp.use(session({ defer: true }));
      // load user if session exists
      dummyApp.use(koaAuth.middleware.loadUser);

      // add auth api
      koaAuth.mount(dummyApp, {
        middleware: {
          register: [function *(next) {
            // add a custom app specific role
            this.request.body.roles = this.request.body.roles || [];
            this.request.body.roles.push('test');

            yield next;
          }]
        }
      });

      // add routes useful for testing
      var router = dummyApp.router();
      dummyApp.mount(router);

      router.get('/user', koaAuth.middleware.assertState({ authenticated: true }), function *() {
        this.body = this.state.user || {};
        this.status = 200;
      }); // jshint ignore:line

      var app = http.createServer(dummyApp.callback());

      request(app)
        .post('/auth/register')
        .send({ email: 'a@aaaaaa.com', pass: 'strong and sexy password' })
        .expect(200)
        .end(function(err) {
          expect(err).to.not.exist; // jshint ignore:line

          request(app)
            .post('/auth/login')
            .send({ email: 'a@aaaaaa.com', pass: 'strong and sexy password' })
            .expect(200)
            .expect(function(res) {
              expect(res.body.roles).to.eql(['user', 'test']);
            }).end(done);
        });
    });

    it('should allow disabling certain routes', function(done) {
      var dummyApp = koa();

      // add session middleware
      dummyApp.use(session({ defer: true }));
      // load user if session exists
      dummyApp.use(koaAuth.middleware.loadUser);

      // add auth api
      koaAuth.mount(dummyApp, {
        routes: { disabled: ['login'] }
      });

      var app = http.createServer(dummyApp.callback());

      request(app)
        .post('/auth/register')
        .send({ email: 'a@aaaaaa.com', pass: 'strong and sexy password' })
        .expect(200)
        .end(function(err) {
          expect(err).to.not.exist; // jshint ignore:line

          request(app)
            .post('/auth/login')
            .send({ email: 'a@aaaaaa.com', pass: 'strong and sexy password' })
            .expect(404)
            .end(done);
        });
    });

    it('should allow disabling certain routes using their path', function(done) {
      var dummyApp = koa();

      // add session middleware
      dummyApp.use(session({ defer: true }));
      // load user if session exists
      dummyApp.use(koaAuth.middleware.loadUser);

      // add auth api
      koaAuth.mount(dummyApp, {
        routes: { disabled: ['/login'] }
      });

      var app = http.createServer(dummyApp.callback());

      request(app)
        .post('/auth/register')
        .send({ email: 'a@aaaaaa.com', pass: 'strong and sexy password' })
        .expect(200)
        .end(function(err) {
          expect(err).to.not.exist; // jshint ignore:line

          request(app)
            .post('/auth/login')
            .send({ email: 'a@aaaaaa.com', pass: 'strong and sexy password' })
            .expect(404)
            .end(done);
        });
    });

    it('should allow enabling only certain routes', function(done) {
      var dummyApp = koa();

      // add session middleware
      dummyApp.use(session({ defer: true }));
      // load user if session exists
      dummyApp.use(koaAuth.middleware.loadUser);

      // add auth api
      koaAuth.mount(dummyApp, {
        routes: { enabled: ['register'] }
      });

      var app = http.createServer(dummyApp.callback());

      request(app)
        .post('/auth/register')
        .send({ email: 'a@aaaaaa.com', pass: 'strong and sexy password' })
        .expect(200)
        .end(function(err) {
          expect(err).to.not.exist; // jshint ignore:line

          request(app)
            .post('/auth/login')
            .send({ email: 'a@aaaaaa.com', pass: 'strong and sexy password' })
            .expect(404)
            .end(function(err) {
              expect(err).to.not.exist; // jshint ignore:line

              request(app)
                .get('/auth/resume')
                .expect(404)
                .end(done);
            });
        });
    });

    it('should allow enabling only certain routes using their path', function(done) {
      var dummyApp = koa();

      // add session middleware
      dummyApp.use(session({ defer: true }));
      // load user if session exists
      dummyApp.use(koaAuth.middleware.loadUser);

      // add auth api
      koaAuth.mount(dummyApp, {
        routes: { enabled: ['/register'] }
      });

      var app = http.createServer(dummyApp.callback());

      request(app)
        .post('/auth/register')
        .send({ email: 'a@aaaaaa.com', pass: 'strong and sexy password' })
        .expect(200)
        .end(function(err) {
          expect(err).to.not.exist; // jshint ignore:line

          request(app)
            .post('/auth/login')
            .send({ email: 'a@aaaaaa.com', pass: 'strong and sexy password' })
            .expect(404)
            .end(function(err) {
              expect(err).to.not.exist; // jshint ignore:line

              request(app)
                .get('/auth/resume')
                .expect(404)
                .end(done);
            });
        });
    });

    it('should support custom routes', function(done) {
      var dummyApp = koa();

      // add session middleware
      dummyApp.use(session({ defer: true }));
      // load user if session exists
      dummyApp.use(koaAuth.middleware.loadUser);

      const TOKENS = {
        SPECIAL20: { discount: 0.2 },
        JABAJABA50: { discount: 0.5 }
      };

      var tokenRegistration = {
        description: 'User registration using tokens',
        path: '/token-register',
        method: 'POST',
        schema: merge(koaAuth.AuthRouter.spec.register.schema, {
          body: {
            properties: {
              token: { type: 'string', required: true, enum: Object.keys(TOKENS) }
            }
          }
        }),
        state: { authenticated: false },
        session: { set: true },
        fn: function *(body) {
          var user = { email: body.email, pass: body.pass };

          // token specific logic
            // in this case, we are attaching a discount rate to a user
          user.properties = {
            discount: TOKENS[body.token].discount
          };

          return yield auth.methods.register(user);
        }
      };

      // add auth api
      koaAuth.mount(dummyApp, {
        routes: {
          custom: { tokenRegistration }
        }
      });

      var app = http.createServer(dummyApp.callback());

      request(app)
        .post('/auth/token-register')
        .send({ email: 'a@aaaaaa.com', pass: 'strong and sexy password', token: 'SPECIAL20' })
        .expect(200)
        .expect(function(res) {
          expect(res.body.properties.discount).to.equal(0.2);
        })
        .end(function(err) {
          expect(err).to.not.exist; // jshint ignore:line

          request(app)
            .post('/auth/login')
            .send({ email: 'a@aaaaaa.com', pass: 'strong and sexy password' })
            .expect(200)
            .expect(function(res) {
              expect(res.body.properties.discount).to.equal(0.2);
            })
            .end(done);
        });
    });
  });

  describe('#middleware', function() {
    describe('#loadUser', function() {
      it('should set the user in ctx.state if session exists', function(done) {
        request(app)
          .post('/auth/login')
          .send({ email: 'a@a.com', pass: 'I like big hashes and I cannot lie' })
          .end(function(err, res) {
            request(app)
              .get('/user')
              .set('X-Session-ID', res.get('X-Session-ID'))
              .expect(function(res) {
                expect(res.body.email).to.equal('a@a.com');
              })
              .end(done);
          });
      });
    });

    describe('#assertState', function() {
      it('should throw an error if authenticated:false is passed with other properties', function() {
        expect(function() {
          koaAuth.middleware.assertState({ authenticated: false, role: 'abc' });
        }).to.throw();

        expect(function() {
          koaAuth.middleware.assertState({ authenticated: false, status: 'enabled' });
        }).to.throw();
      });

      // maybe someone wants to set custom statuses?
      it.skip('should throw an error if an invalid status is passed', function() {
        expect(function() {
          koaAuth.middleware.assertState({ status: 'wateva' });
        }).to.throw();
      });

      it('should return 401 if user is not authenticated', function(done) {
        request(app)
          .get('/session')
          .expect(401)
          .end(done);
      });

      it('should return 403 if user status does not match', function(done) {
        request(app)
          .post('/auth/login')
          .send({ email: 'a@a.com', pass: 'I like big hashes and I cannot lie' })
          .end(function(err, res) {
            request(app)
              .get('/status/registered')
              .set('X-Session-ID', res.get('X-Session-ID'))
              .expect(403)
              .end(done);
          });
      });

      it('should return 200 if user status matches - #1', function(done) {
        request(app)
          .post('/auth/login')
          .send({ email: 'b@b.com', pass: 'I like big hashes and I cannot lie' })
          .end(function(err, res) {
            request(app)
              .get('/status/registered')
              .set('X-Session-ID', res.get('X-Session-ID'))
              .expect(200)
              .end(done);
          });
      });

      it('should return 200 if user status matches - #2', function(done) {
        request(app)
          .post('/auth/login')
          .send({ email: 'b@b.com', pass: 'I like big hashes and I cannot lie' })
          .end(function(err, res) {
            request(app)
              .get('/status/registered-or-enabled')
              .set('X-Session-ID', res.get('X-Session-ID'))
              .expect(200)
              .end(done);
          });
      });

      it('should return 200 if user status matches - #3', function(done) {
        request(app)
          .post('/auth/login')
          .send({ email: 'a@a.com', pass: 'I like big hashes and I cannot lie' })
          .end(function(err, res) {
            request(app)
              .get('/status/registered-or-enabled')
              .set('X-Session-ID', res.get('X-Session-ID'))
              .expect(200)
              .end(done);
          });
      });

      it('should return 403 if user does not match role', function(done) {
        request(app)
          .post('/auth/login')
          .send({ email: 'a@a.com', pass: 'I like big hashes and I cannot lie' })
          .end(function(err, res) {
            request(app)
              .get('/role/boss')
              .set('X-Session-ID', res.get('X-Session-ID'))
              .expect(403)
              .end(done);
          });
      });

      it('should return 403 if user does not exist in roles', function(done) {
        request(app)
          .post('/auth/login')
          .send({ email: 'a@a.com', pass: 'I like big hashes and I cannot lie' })
          .end(function(err, res) {
            request(app)
              .get('/role/boss-or-manager')
              .set('X-Session-ID', res.get('X-Session-ID'))
              .expect(403)
              .end(done);
          });
      });

      it('should return 200 if user role matches', function(done) {
        var user = {
          email: 'd@d.com',
          hash: '$2a$10$jmUBONzZVmZoWh7GFhIeC.RiDnf.9rRExVO8ActVwdmgKTeJkeODy',
          roles: ['user', 'boss'],
          status: 'disabled',
          properties: {
            name: 'Person D',
            __pass: 'I like big hashes and I cannot lie'
          }
        };

        co(function *() {
          yield auth.db.insert(user);
        }).then(function(err) {
          expect(err).to.not.exist; // jshint ignore:line

          request(app)
            .post('/auth/login')
            .send({ email: 'd@d.com', pass: 'I like big hashes and I cannot lie' })
            .end(function(err, res) {
              request(app)
                .get('/role/boss-or-manager')
                .set('X-Session-ID', res.get('X-Session-ID'))
                .expect(200)
                .end(done);
            });
        }, done);
      });

      it('should return 200 if user role and status matches - #1', function(done) {
        var user = {
          email: 'd@d.com',
          hash: '$2a$10$jmUBONzZVmZoWh7GFhIeC.RiDnf.9rRExVO8ActVwdmgKTeJkeODy',
          roles: ['user', 'boss'],
          status: 'enabled',
          properties: {
            name: 'Person D',
            __pass: 'I like big hashes and I cannot lie'
          }
        };

        co(function *() {
          yield auth.db.insert(user);
        }).then(function(err) {
          expect(err).to.not.exist; // jshint ignore:line

          request(app)
            .post('/auth/login')
            .send({ email: 'd@d.com', pass: 'I like big hashes and I cannot lie' })
            .end(function(err, res) {
              request(app)
                .get('/status/enabled/role/boss')
                .set('X-Session-ID', res.get('X-Session-ID'))
                .expect(200)
                .end(done);
            });
        }, done);
      });

      it('should return 200 if user role and status matches - #2', function(done) {
        var user = {
          email: 'd@d.com',
          hash: '$2a$10$jmUBONzZVmZoWh7GFhIeC.RiDnf.9rRExVO8ActVwdmgKTeJkeODy',
          roles: ['user', 'boss'],
          status: 'registered',
          properties: {
            name: 'Person D',
            __pass: 'I like big hashes and I cannot lie'
          }
        };

        co(function *() {
          yield auth.db.insert(user);
        }).then(function(err) {
          expect(err).to.not.exist; // jshint ignore:line

          request(app)
            .post('/auth/login')
            .send({ email: 'd@d.com', pass: 'I like big hashes and I cannot lie' })
            .end(function(err, res) {
              request(app)
                .get('/status/registered-or-enabled/role/boss-or-manager')
                .set('X-Session-ID', res.get('X-Session-ID'))
                .expect(200)
                .end(done);
            });
        }, done);
      });
    });
  });

  describe('#hooks', function() {
    it('should run beforeFn hook', function(done) {
      var called = false;

      authRouter.hooks.beforeFn.add(function *(id, args, route) {
        expect(this).to.have.property('assert');
        expect(this).to.have.property('request');
        expect(this).to.have.property('response');

        expect(id).to.equal('login');

        expect(args).to.have.property('email', 'a@a.com');
        expect(args).to.have.property('pass', 'I like big hashes and I cannot lie');

        expect(route).to.have.property('id', 'login');
        expect(route).to.have.property('fn');

        called = true;
      });

      request(app)
        .post('/auth/login')
        .send({ email: 'a@a.com', pass: 'I like big hashes and I cannot lie' })
        .end(function(err, res) {
          expect(called).to.equal(true);
          done();
        });
    });

    it('should run afterFn hook', function(done) {
      var called = false;

      authRouter.hooks.afterFn.add(function *(id, args, route) {
        expect(this).to.have.property('assert');
        expect(this).to.have.property('request');
        expect(this).to.have.property('response');
        expect(this.body).to.have.property('_id');
        expect(this.body).to.have.property('status');

        expect(id).to.equal('login');

        expect(args).to.have.property('email', 'a@a.com');
        expect(args).to.have.property('pass', 'I like big hashes and I cannot lie');

        expect(route).to.have.property('id', 'login');
        expect(route).to.have.property('fn');

        called = true;
      });

      request(app)
        .post('/auth/login')
        .send({ email: 'a@a.com', pass: 'I like big hashes and I cannot lie' })
        .end(function(err, res) {
          expect(called).to.equal(true);
          done();
        });
    });
  });
});