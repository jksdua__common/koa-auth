'use strict';

const SUPPORTED_VERSIONS = '~7.0.x';

var assert = require('assert');

var merge = require('lodash.merge');
var debug = require('debug');

var appDebug = debug('@knoxxnxt/koa-auth:app');
var middlewareDebug = debug('@knoxxnxt/koa-auth:middleware');

function _roleAllowed(actual, required) {
	return actual.indexOf(required) > -1;
}

function roleAllowed(actual, required) {
	middlewareDebug('roleAllowed: actual - %j, required - %j', actual, required);

	if (Array.isArray(required)) {
		return required.some(function(oneReq) {
			return _roleAllowed(actual, oneReq);
		});
	} else {
		return _roleAllowed(actual, required);
	}
}

function _statusAllowed(actual, required) {
	return actual === required;
}

function statusAllowed(actual, required) {
	middlewareDebug('statusAllowed: actual - %j, required - %j', actual, required);

	if (Array.isArray(required)) {
		return required.some(function(oneReq) {
			return _statusAllowed(actual, oneReq);
		});
	} else {
		return _statusAllowed(actual, required);
	}
}

module.exports = function(auth) {
	// ensure auth instance passed is valid
	assert(auth.isSupported(SUPPORTED_VERSIONS));

	var api = { middleware: {} };

	api.middleware = {
		loadUser: function *(next) {
			var user = this.state.user;
			var session = yield this.session;

			middlewareDebug('loadUser: user - %j, session - %j', user, session);

			if (!user) {
				if (session && session.email) {
					// assumes email exists in session data
						// automatically added if auth app is generated here
						// needs to be manually added otherwise
					this.state.user = yield auth.methods.get({ email: session.email });
					this.assert(this.state.user, 500);
				} else {
					// setting it to null asserts that the function was run but no
					this.state.user = null;
				}
			}

			yield next;
		},
		// asserts 3 items
			// state.authenticated, state.role, state.status
		assertState: function(requiredState) {
			// assert passed options are valid
			if (requiredState.status || requiredState.role) {
				assert.notEqual(requiredState.authenticated, false, 'state.authenticated cannot be `false` if state.status or state.role is passed');
			}

			// defaults
			if (requiredState.status || requiredState.role) {
				requiredState.authenticated = true;
			}

			// skip state checking
			if (requiredState.skip) {
				return function *(next) { yield next; };
			}

			return function *(next) {
				var user = this.state.user;
				var userState = {
					authenticated: !!user,
					status: user && user.status,
					roles: user && user.roles
				};

				middlewareDebug('assertState: userState - %j, requiredState - %j', userState, requiredState);

				this.assert(userState.authenticated === requiredState.authenticated, 401);

				// the other checks only apply if user is authenticated
				if (userState.authenticated) {
					if (requiredState.status) {
						this.assert(statusAllowed(userState.status, requiredState.status), 403);
					}

					if (requiredState.role) {
						this.assert(roleAllowed(userState.roles, requiredState.role), 403);
					}
				}

				yield next;
			}; // jshint ignore:line
		}
	};

	function addRouteStatus(spec, config) {
		// aliases
		config.enabled = config.enabled || config.enable;
		config.disabled = config.disabled || config.disable;

		assert(!(config.enabled && config.disabled), 'Cannot use both `enable` and `disable` for routes config.');

		for (var i in spec) {
			spec[i].enabled = true;
		}

		// disable all routes, except for those enabled
		if (config.enabled) {
			for (var j in spec) {
				if (config.enabled.indexOf(j) === -1 && config.enabled.indexOf(spec[j].path) === -1) {
					spec[j].enabled = false;
				}
			}
		}

		if (config.disabled) {
			for (var k in spec) {
				if (config.disabled.indexOf(k) > -1 || config.disabled.indexOf(spec[k].path) > -1) {
					spec[k].enabled = false;
				}
			}
		}
	}

	function AuthRouter(app, opt) {
		opt = this.opt = merge(
			{
				prefix: '/auth',
				// add middleware for each method as an array
					// eg: login: [function *(next) { ... }, function *(next) { ... }]
				middleware: {},
				// schema extensions for paths
					// eg: login: { params: { properties: { ... } } }
				schema: {},
				// configure what routes are loaded
				routes: { enable: null, disable: null, enabled: null, disabled: null, custom: {} },
        hooks: { beforeFn: new Set(), afterFn: new Set() }
			},
			opt);
		appDebug('options: %j', opt);

		this.app = app;
		var router = this.router = app.router({
			prefix: opt.prefix
		});
		app.mount(router);

		// unpack user if a session exists
		router.use(api.middleware.loadUser);

		var spec = this.spec = AuthRouter.spec;
		addRouteStatus(spec, opt.routes);

		for (var id in spec) {
			if (spec[id].enabled) {
				this.addRoute(id, spec[id]);
			}
		}

		for (id in opt.routes.custom) {
			this.addRoute(id, opt.routes.custom[id]);
		}

    this.hooks = opt.hooks;
	}

	// @static
	AuthRouter.spec = require('@knoxxnxt/auth-http-spec')(auth);

	AuthRouter.prototype.addRoute = function(id, item) {
		var opt = this.opt, app = this.app, router = this.router;
		var args = [item.path, api.middleware.assertState(item.state)];

    item.id = id;

		// mark item as enabled
		item.enabled = true;

		// extend schema if overrides are supplied by the user
		if (item.schema || opt.schema[id]) {
			item.schema = merge({}, item.schema, opt.schema[id]);

			// if no schema
			if (Object.keys(item.schema).length) {
				args.push(app.schema(item.schema));
			}
		}

		// add any custom middleware
		if (opt.middleware[id]) {
			args = args.concat(opt.middleware[id]);
		}

		// set session if spec requires it
		if (item.session && item.session.set) {
			args.push(this.setSession);
		}

		// destroy session if spec requires it
		if (item.session && item.session.unset) {
			args.push(this.unsetSession);
		}

		if (item.session && item.session.resume) {
			args.push(this.resumeSession);
		}

		// if calls a method, create executor
		if (item.method) {
			args.push(this.executorFn(item));
		}

		router[item.method.toLowerCase()].apply(router, args);
	};

	AuthRouter.prototype.executorFn = function executorFn(item) {
		var fromSession = item.session && item.session.args;
    var router = this;

		return function *() {
			var args = merge({}, this.params, this.query, this.request.body);

			if (fromSession) {
				var session = yield this.session;
				fromSession.forEach(function(argName) {
					args[argName] = session[argName];
				});
			}

			appDebug('route: path - %s, originalUrl - %s, args - %j', this.path, this.originalUrl, args);
      for (let hook of router.hooks.beforeFn) {
        yield hook.call(this, item.id, args, item);
      }

			if (item.fn) {
				this.body = yield item.fn.call(this, args);
			}

      for (let hook of router.hooks.afterFn) {
        yield hook.call(this, item.id, args, item);
      }

			appDebug('route: this.body - %j', this.body);
		};
	};

	AuthRouter.prototype.setSession = function *setSession(next) {
		appDebug('setSession: path - %s, originalUrl - %s', this.path, this.originalUrl); // jshint ignore:line

		yield next;

		// this.body represents the user object as returned by the relevant auth method
		var user = this.body; // jshint ignore:line

		var session = yield this.session; // jshint ignore:line
		session.email = user.email;

		appDebug('setSession: user - %j, session - %j', user, session);
	};

	AuthRouter.prototype.unsetSession = function *unsetSession(next) {
		appDebug('unsetSession: path - %s, originalUrl - %s', this.path, this.originalUrl); // jshint ignore:line

		yield next;

		this.session = null; // jshint ignore:line
		this.body = { success: true }; // jshint ignore:line

		appDebug('unsetSession: user - %j', this.state.user); // jshint ignore:line
	};

	AuthRouter.prototype.resumeSession = function *resumeSession() {
		appDebug('resumeSession: path - %s, originalUrl - %s', this.path, this.originalUrl); // jshint ignore:line

		var user = this.state.user; // jshint ignore:line
			appDebug('resumeSession: user - %j', user);

		this.assert(user, 401); // jshint ignore:line
		this.body = user; // jshint ignore:line
	} // jshint ignore:line

	api.AuthRouter = AuthRouter;

	// automatically create a koa router for all the auth methods
	api.mount = function(app, opt) {
		return new AuthRouter(app, opt);
	};

	return api;
};
